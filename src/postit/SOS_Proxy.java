package postit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
 
/**
 * Listens to changes in destination folder
 * 
 * To test this class:
 * 1. please change : FOLDER_PATH to directory that you need
 * 2. when app is started, do create some new file in destination folder and notice the change in console
 * @author dren
 *
 */
@SuppressWarnings("deprecation")
public class SOS_Proxy extends Thread {
	
	static final String XML_EXTENSION = ".xml";
	static final String EXI_APP = "application/exi";
	static final String XML_APP = "application/xml";
	
	static Properties props;
	
    private Set<File> fileSet = new HashSet<File>();
     
    /*set it on false from outside when you want to stop*/
    private boolean runSignal = true;
     
    /*path to folder where to listen to file existence*/
    private static String SRC_FOLDER_PATH = "src_files";
    
    /*path to folder where to copy the file*/
    private static String DEST_FOLDER_PATH = "dest_files";
    
    /*SOS URL path to insert data*/
    private static String SOS_URL = "http://localhost/52n-sos-webapp/service/pox";
     
     
    /*sleep time*/
    private final long SLEEP_TIME = 500; //0.5 sec.
     
    public void run() { 	
         
        while(runSignal){
            //System.out.println("Let's check is there a new file...");
             
            /*destination folder*/
            File folder = new File(SRC_FOLDER_PATH);
             
            /*check file names in dir*/
            if (folder.isDirectory()){
                File[] fileList = folder.listFiles();
                 
                for(File one : fileList){
                    boolean isNewFile = fileSet.add(one);
                     
                    if (isNewFile)
                    {
                    	if(one.isFile()){
	                    	System.out.println("* " + one.getName());
	                    	
	                    	if(XMLDataPost(SRC_FOLDER_PATH + "/" + one.getName())){
	                    		if(one.renameTo(new File(DEST_FOLDER_PATH +  "/" + one.getName()))){
	                    			System.out.println("File is moved successful!");
	                    		}else{
	                    			System.out.println("File is failed to move!");
	                    		}
	                    	}
                    	}
                    }
                }
            }
             
            /*go to sleep*/
            doSleep();
                 
        }//while
     
    }
     
     
    /**
     * sleep between listening folder iteration
     */
    private void doSleep(){
        try {
            //System.out.println("I go to sleep for 1 sec now...");
            sleep(SLEEP_TIME);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
     
 
    /**
     * stop running this thread when application stops
     */
    public void stopRunning() {
        System.out.println("Bye!");
        this.runSignal = false;
    }
 
    @SuppressWarnings({ "resource" })
	private static boolean  XMLDataPost(String fileName){
    	 
        boolean success = false;

		HttpClient httpclient = new DefaultHttpClient();
 
        try {
 
            File file = new File(fileName.trim());
            String sos_url_binding = null;
            InputStreamEntity reqEntity = new InputStreamEntity(new FileInputStream(file), file.length());
            if(fileName.contains(XML_EXTENSION)){
            	reqEntity.setContentType(XML_APP);
            	sos_url_binding = SOS_URL +"/pox";
            }else{
            	reqEntity.setContentType(EXI_APP);
            	sos_url_binding=SOS_URL +"/exi";
            }

            //reqEntity.setChunked(true);
            
            HttpPost httpPost = new HttpPost(sos_url_binding);
            
            httpPost.setEntity(reqEntity);
 
            System.out.println("Executing request " + httpPost.getRequestLine());
            HttpResponse response = httpclient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();
 
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());

            if(response.getStatusLine().getStatusCode() == 200){
                success = true;
            }
            if (resEntity != null) {
                System.out.println("Response content length: " + resEntity.getContentLength());
            	InputStream instream = resEntity.getContent();
                FileOutputStream output = new FileOutputStream("response.txt");
                try {
                    int l;
                    byte[] tmp = new byte[2048];
                    while ( (l = instream.read(tmp)) != -1 ) {
                        output.write(tmp, 0, l);
                    }
                } finally {
                    output.close();
                    instream.close();
                }
            }
            EntityUtils.consume(resEntity);
        } 
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            httpclient.getConnectionManager().shutdown();
        }
 
        return success;
 
    }  
     
    public static void main(String[] args) {
        
        try {
        	props = new Properties();
			props.load(new FileInputStream("xmlProperties.properties"));
			SOS_URL = props.getProperty("post_url").trim();
			if(SOS_URL.equals(null)){
				// Print usage and exit
			    System.out.println("Missing SOS URL property: <post_url>");
			    System.exit(1);
			}
			SRC_FOLDER_PATH = props.getProperty("src_path").trim();
			if(SRC_FOLDER_PATH.equals(null)){
				// Print usage and exit
			    System.out.println("Missing source folder path property: <src_path>");
			    System.exit(1);
			}
			DEST_FOLDER_PATH = props.getProperty("dest_path").trim();
			if(DEST_FOLDER_PATH.equals(null)){
				// Print usage and exit
			    System.out.println("Missing destination folder path property: <dest_path>");
			    System.exit(1);
			}
			
			File directory = new File(DEST_FOLDER_PATH);
			if (!directory.exists()){
				System.out.println("make destination directory");
				directory.mkdir();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        SOS_Proxy fileListener = new SOS_Proxy();
        fileListener.start();
    }
     
}